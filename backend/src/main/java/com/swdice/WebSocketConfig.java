package com.swdice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

import java.util.List;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    Logger log = LogManager.getLogger(WebSocketConfig.class);

    /**
     * Configure endpoints used to establish a websocket connection.
     * @param registry registry of stomp endpoints
     */
    public void registerStompEndpoints(StompEndpointRegistry registry) {
     registry.addEndpoint("/ws-connection").withSockJS();
    }

    /**
     * Configure message broker channel prefixes, application prefixes route to controllers first.
     * @param registry
     */
    public void configureMessageBroker(org.springframework.messaging.simp.config.MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/topic/");
        registry.setApplicationDestinationPrefixes("/app");
    }
}

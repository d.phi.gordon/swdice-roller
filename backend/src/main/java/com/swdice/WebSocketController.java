package com.swdice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {
    Logger log = LogManager.getLogger(WebSocketController.class);

    /**
     * Receives STOMP SEND messages prefixed with '/app/messages',
     * routed to message broker channel '/topic/messages'
     */
    @MessageMapping("/messages")
    @SendTo("/topic/messages")
    public String handlePublicMessage(@Payload String message){
        log.info("Message Received: " + message);
        return message;
    }
}

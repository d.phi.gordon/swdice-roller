import React from 'react'
import ReactDOM from 'react-dom'
import SWDice from '../SWDice'
import { shallow, mount } from 'enzyme'
import { DiceFaces, DiceTypes } from "../../Dice"
import { act } from 'react-dom/test-utils'

test("works", () => {
  expect(true)
})

test.todo("set name")
test.todo("set room")
test.todo("adding and removing dice")
test("component renders without error", () => {
  const component = mount(<SWDice />)
})

xtest("force dice results are added to force side tokens", done => {

  const container = mount(<SWDice />)
  const forceTokens = container.find('#force-tokens-display')
  const rollButton = container.find('button#roll')
  const forceDiceButton = container.find('#genesys-dice-force')

  act(() =>forceDiceButton.prop('onClick')())

    act(() => rollButton.prop('onClick')())

    container.update()
    expect(container.find('#force-token').length).toBeGreaterThan(0)
    //console.log(forceDiceButton.debug())
    // container.update(() => console.log(forceDiceButton.debug()))

  done()
})

test("alternative", ()=> {
  const container = document.createElement('div')
  document.body.appendChild(container)

  act(
    () => {
      ReactDOM.render(<SWDice/>, container)
    }
  )
  const button = container.querySelector('#genesys-dice-force')
  console.log(button.textContent)

  act(()=>{
    button.dispatchEvent(new MouseEvent('click', {bubbles: true}))
  })
  console.log(button.textContent)
})

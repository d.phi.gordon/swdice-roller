// const Dice = require(".");

// function Polyhedral1() {
//   diceFace.call(this);
//   this.value = 1;
// }
// Polyhedral1.prototype = Object.create(diceFace.prototype);

// function Polyhedral2() {
//   diceFace.call(this);
//   this.value = 2;
// }
// Polyhedral2.prototype = Object.create(diceFace.prototype);

class Dice {
  faces: Array<string> | Array<number>
  constructor(faces: Array<string> | Array<number>) {
    this.faces = faces
  }
  roll(): string | number | null {
   return this.faces.length > 0
    ? this.faces[Math.floor(Math.random() * this.faces.length)]
    : null;
  }

}

class D4 extends Dice {
  constructor(){
   super([1, 2, 3, 4])
  }
}


// testing

let d = new Dice(["test", "test2"]);
let d2 = new Dice(["test", "test2"]);

let dArr = Array(2).fill(new Dice([]));

let dtest = new D4();

console.log(dtest.faces);

/*
requirements:

each dice type should not be equal under equality test:
  (diceBuilder().makeD6() === diceBuilder().makeD12()) => false

each dice face should not be equal under equality test:
  (diceFace1 === diceFace2) => false

each dice instance is 'instanceof' dice

dice subtypes are instancesof dice but not siblings
*/

export function DiceBuilder() { }
